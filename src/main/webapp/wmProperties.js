var _WM_APP_PROPERTIES = {
  "activeTheme" : "autumn",
  "defaultLanguage" : "en",
  "displayName" : "MyFirstProject",
  "homePage" : "Main",
  "name" : "MyFirstProject",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};